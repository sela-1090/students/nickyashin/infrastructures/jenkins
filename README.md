# install jenkis 

# Adding the Jenkins chart repository
helm repo add jenkins https://charts.jenkins.io
helm repo update

# Deploying a simple Jenkins instance
helm upgrade --install myjenkins jenkins/jenkins

# Accessing the Jenkins instance
# could not find admin-password using: 
kubectl exec --namespace cicd -it svc/myjenkins -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo
# i searched the file chart-admin-password and found it 

# i created file named "values.yaml" 
controller:
  serviceType: LoadBalancer

# ran the next command to upgrade the Helm release using the values:
helm upgrade --install -f values.yaml myjenkins jenkins/jenkins --namespace cicd

# i ran this command to see the public ip:
kubectl get svc --namespace cicd myjenkins --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}"

# for now its pending 

# to install plugins i changed the values.yaml
ccontroller:
  serviceType: LoadBalancer
  additionalPlugins:
  - octopusdeploy:3.1.6
  - git:5.1.0
  - docker:1.2.2

# then i ran the command to update 
helm upgrade --install -f values.yaml myjenkins jenkins/jenkins --namespace cicd

# log in 
kubectl --namespace cicd port-forward svc/myjenkins 8080:8080
localhost:8080

# to unistall jenkis you need to open control panel and click uninstall a progrem then search jenkus and delete it. 
# or use the command: 
helm uninstall myjenkins --namespace cicd
kubectl delete namespace cicd
helm repo remove jenkins

--------------------------------------------------------------------------------------------------

# installtion jenkis in linux 


# Adding the Jenkins chart repository
helm repo add jenkins https://charts.jenkins.io
helm repo update

# Deploying a simple Jenkins instance
helm upgrade --install myjenkins jenkins/jenkins

# Accessing the Jenkins instance
# find admin-password using: 
kubectl exec --namespace cicd -it svc/myjenkins -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo

# create file named "values.yaml" 
echo 'controller:
  serviceType: LoadBalancer' > values.yaml

# upgrade the Helm release using the values:
helm upgrade --install -f values.yaml myjenkins jenkins/jenkins

# get the public ip:
kubectl get svc --namespace cicd myjenkins --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}"

# update the values.yaml for installing plugins
echo 'controller:
  serviceType: LoadBalancer
  additionalPlugins:
  - octopusdeploy:3.1.6
  - git:5.1.0
  - docker:1.2.2' > values.yaml

# update the Helm release
helm upgrade --install -f values.yaml myjenkins jenkins/jenkins

# to uninstall Jenkins you can use the following command:
helm uninstall myjenkins --namespace cicd
kubectl delete namespace cicd
helm repo remove jenkins


